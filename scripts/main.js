const SQUARE_SIDE_LEN = 5;

const elDivSquares = document.querySelector('#squares');
// Element für Quadrate Tabelle erstellen und
// div für Quadrate hinzufügen
const elTableDiv = document.createElement('div');
elTableDiv.classList.add('table');
elDivSquares.appendChild(elTableDiv);

// Zeile (die Qudarate enthält) erstellen und Tabelle hinzufügen
for (let i = 0; i < SQUARE_SIDE_LEN; i++) {
  const elRowDiv = document.createElement('div');
  elRowDiv.classList.add('row');
  elTableDiv.appendChild(elRowDiv);
  // Spalten (Quadrate) erstellen und Zeile hinzufügen
  for (let j = 0; j < SQUARE_SIDE_LEN; j++) {
    const elCellDiv = document.createElement('div');
    elCellDiv.classList.add('cell');
    elCellDiv.textContent = i * SQUARE_SIDE_LEN + j + 1;
    elRowDiv.appendChild(elCellDiv);
  }
}

// Titel Element selektieren und Text anpassen
const elH1 = document.querySelector('h1');
elH1.textContent = SQUARE_SIDE_LEN + 'x' + SQUARE_SIDE_LEN + '-Quadrat';
